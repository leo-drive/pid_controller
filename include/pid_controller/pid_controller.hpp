// Copyright 2021 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2021 The Autoware Foundation
/// \file
/// \brief This file defines the pid_controller class.

#ifndef PID_CONTROLLER__PID_CONTROLLER_HPP_
#define PID_CONTROLLER__PID_CONTROLLER_HPP_

#include <pid_controller/visibility_control.hpp>
#include <pid_controller/utility.h>

#include <limits>
#include <cstdint>
#include <iostream>



namespace autoware {
    namespace pid_controller {
        class PID_CONTROLLER_PUBLIC PidController {

        public:
            explicit PidController(float kp, float ki, float kd, float _dt, bool enable);

            float calculate(float error);

            void enable() { is_pid_enabled = true; }

            void disable() { is_pid_enabled = false; }

            bool get_status() const { return is_pid_enabled; }

            float get_kp() const { return cons.P; }

            float get_ki() const { return cons.I; }

            float get_kd() const { return cons.D; }

            float get_dt() const { return dt; }

            void set_integral(float fval) { integral_prev_ = fval; }

            void set_constants(float _p, float _i, float _d) { cons = {_p, _i, _d}; }

            void set_output_limits(const float& min, const float& max);

            void set_integral_limits(const float& min, const float& max);

//            void set_derivative_limits(float min, float max);

            void set_time_interval(float _dt) { dt = _dt; }

        private:
            bool is_pid_enabled;

            float dt; // time interval of calculation

            struct constants{
                float P;
                float I;
                float D;
            } cons;

            float integral_prev_;
            float error_prev_;

            bool is_out_limit_set_;
            float output_max_;
            float output_min_;

            bool is_integral_limit_set_;
            float integral_max_;
            float integral_min_;
        };

    }  // namespace pid_controller
}  // namespace autoware

#endif  // PID_CONTROLLER__PID_CONTROLLER_HPP_
