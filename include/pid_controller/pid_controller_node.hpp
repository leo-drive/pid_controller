// Copyright 2021 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2021 The Autoware Foundation
/// \file
/// \brief This file defines the pid_controller_node class.

#ifndef PID_CONTROLLER__PID_CONTROLLER_NODE_HPP_
#define PID_CONTROLLER__PID_CONTROLLER_NODE_HPP_


#include <autoware_auto_msgs/msg/vehicle_kinematic_state.hpp>
#include <autoware_auto_msgs/msg/vehicle_control_command.hpp>
#include <autoware_auto_msgs/msg/trajectory.hpp>
#include <autoware_auto_msgs/msg/route.hpp>
#include <tf2_msgs/msg/tf_message.hpp>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Quaternion.h>

#include <geometry_msgs/msg/pose_stamped.hpp>
#include <visualization_msgs/msg/marker.hpp>


#include <pid_controller/pid_controller.hpp>

#include <rclcpp/rclcpp.hpp>

#include <chrono>
#include <ctime>
#include <math.h>

using Command = autoware_auto_msgs::msg::VehicleControlCommand;
using State = autoware_auto_msgs::msg::VehicleKinematicState;
using Trajectory = autoware_auto_msgs::msg::Trajectory;
using TFMessage = tf2_msgs::msg::TFMessage_<std::allocator<void>>;
using Route = autoware_auto_msgs::msg::Route_<std::allocator<void>>;
using Pose = geometry_msgs::msg::PoseStamped;

namespace autoware {
    namespace pid_controller {


        class PID_CONTROLLER_PUBLIC PidControllerNode : public rclcpp::Node {
        public:

            explicit PidControllerNode(const rclcpp::NodeOptions &options);


        private:

            rclcpp::Subscription<TFMessage>::SharedPtr m_sub_tf2_{};
            rclcpp::Subscription<TFMessage>::SharedPtr m_sub_tf2_static_{};
            rclcpp::Subscription<State>::SharedPtr m_sub_veh_state_{};
            rclcpp::Subscription<Trajectory>::SharedPtr m_sub_traj_{};

            rclcpp::Publisher<Command>::SharedPtr m_pub_veh_cmd_;
            rclcpp::Publisher<Trajectory>::SharedPtr m_pub_traj_debug_;
            rclcpp::Publisher<Pose>::SharedPtr m_debug_cp_;
            rclcpp::Publisher<Pose>::SharedPtr m_debug_gp_;

            rclcpp::TimerBase::SharedPtr timer_;


            struct Line {
                double a{}, b{}, c{};
            };

            struct Eulers {
                double roll, pitch, yaw;
            } m_eulers{};

            void on_state(const State::SharedPtr &msg);

            void on_tf(const TFMessage::SharedPtr &msg);

            void on_tf_static(const TFMessage::SharedPtr &msg);

            void on_trajectory(const Trajectory::SharedPtr &msg);

            void publish_control_command(float str_angle, float long_acc)const;

            Trajectory get_computed_trajectory(const Trajectory &);

            Pose get_control_point(const TFMessage::SharedPtr &msg);

            Pose get_goal_point(size_t closest_point_idx_) const;

            float calculate_lat_error(const Pose &gp, const Pose &cp);

            float calculate_lon_error(size_t goal_traj_idx) const;

            static double distance_to_line(const Line &, double, double) ;

            static double get_distance(const Pose &, const Pose &);// calculates distance between goal point and control point
            // this distance stands for lateral error

            static Line get_line(const Pose& rps, double angle_rad) ;

            size_t find_closest_traj() const;

            static Eulers get_euler_angles(tf2::Quaternion &) ;

            void control_step();


            TFMessage m_map2odom_static_tf{};
            TFMessage m_odom2base_tf{};
            Trajectory m_reference_trajectory{};
            Trajectory m_computed_trajectory{};
            Pose m_control_point{};
            Pose m_goal_point{};
            State m_last_state{};

            float m_max_steering_angle{0.331f};

            double m_control_point_radius{5.0};

            PidController LatPID_;
            PidController LonPID_;

        };
    }  // namespace pid_controller
}  // namespace autoware

#endif  // PID_CONTROLLER__PID_CONTROLLER_NODE_HPP_
