//
// Created by ozkan on 12.08.2021.
//

#ifndef BUILD_UTILITY_H
#define BUILD_UTILITY_H

namespace utility{

    constexpr float& clamp(const float& val, const float& lo, const float& ho){
        if(val > ho){
            return const_cast<float&>(ho);
        }
        else if(val < lo){
            return const_cast<float&>(lo);
        }
        return const_cast<float&>(val);
    }


}

#endif //BUILD_UTILITY_H
