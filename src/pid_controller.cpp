// Copyright 2021 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "pid_controller/pid_controller.hpp"


namespace autoware {
    namespace pid_controller {

        PidController::PidController(float kp, float ki, float kd, float _dt, bool enable) :
                is_pid_enabled{enable},
                dt{_dt},
                cons{kp, ki, kd},
                integral_prev_{0.0f},
                error_prev_{0.0f},
                is_out_limit_set_{false},
                output_max_{std::numeric_limits<float>::max()},
                output_min_{std::numeric_limits<float>::min()},
                is_integral_limit_set_{false},
                integral_max_{std::numeric_limits<float>::max()},
                integral_min_{std::numeric_limits<float>::min()}
                {}

        float PidController::calculate(float error) {
            if (is_pid_enabled) // is pid enabled
            {
                // Proportion Calculations
                const auto p_response{error * cons.P};

                // Derivative Calculations
                const auto derivative_value{(error - error_prev_) / dt};

                const auto d_response{derivative_value * cons.D};

                // Sum proportion and derivative response to decide whether integral is necessary
                const auto pd_response{p_response + d_response};

                // Integral calculations
                float integral_value{0.0f};
                if (is_out_limit_set_ &&
                    (pd_response < output_max_) &&
                    (pd_response > output_min_)) {

                    integral_value = integral_prev_ + (error * dt);

                    if (is_integral_limit_set_) { // Apply the integral limits
                        integral_value = utility::clamp(
                                integral_value, integral_min_, integral_max_);
                    }
                }

                // Calculate output
                auto output = pd_response + integral_value * cons.I;

                // Check output limits
                if (is_out_limit_set_) {
                    output = utility::clamp(output, output_min_, output_max_);
                }

                error_prev_ = error;
                integral_prev_ = integral_value;

                return output;
            } else {
                return 0.0f;
            }
        }

        void PidController::set_output_limits(const float& min, const float& max) {
            is_out_limit_set_ = true;
            output_max_ = max;
            output_min_ = min;
            if (!is_integral_limit_set_){
                set_integral_limits(min / cons.I , max / cons.I);
            }
        }

        void PidController::set_integral_limits(const float& min, const float& max) {
            is_integral_limit_set_ = true;
            integral_max_ = max;
            integral_min_ = min;
        }

//        void PID::set_derivative_limits(float min, float max) {
//            derivative.limit.flag = true;
//            derivative.limit.max = max;
//            derivative.limit.min = min;
//        }


    }  // namespace pid_controller
}  // namespace autoware
