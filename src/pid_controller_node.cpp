// Copyright 2021 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "pid_controller/pid_controller_node.hpp"
#include <chrono>


namespace autoware {
    namespace pid_controller {

        PidControllerNode::PidControllerNode(const rclcpp::NodeOptions &options)
                : Node("pid_controller", options),
                  LatPID_{0.02f, 0.001f, 0.001f, 0.1f, true},
                  LonPID_{0.5f, 0.001f, 0.0001f, 0.1f, true}{

            LatPID_.set_output_limits(-m_max_steering_angle, m_max_steering_angle);

            LonPID_.set_output_limits(-3, 3);


            const std::string tf_topic = "/tf";
            const std::string tf_static_topic = "/tf_static";
            const std::string state_topic = "/vehicle/vehicle_kinematic_state";
            const std::string traj_topic = "/planning/trajectory";
            const std::string command_topic = "/vehicle/vehicle_command";
            const std::string traj_debug_topic = "/control/calculated_traj_debug";
            const std::string cp_debug_topic = "/control/cp_debug";// cp: control point
            const std::string gp_debug_topic = "/control/gp_debug";// gp: goal point


            using SubAllocT = rclcpp::SubscriptionOptionsWithAllocator<std::allocator<void>>;
            m_sub_veh_state_ = create_subscription<State>(
                    state_topic, rclcpp::QoS{10},
                    [this](const State::SharedPtr msg) { on_state(msg); },
                    SubAllocT{});

            m_sub_traj_ = create_subscription<Trajectory>(
                    traj_topic, rclcpp::QoS{10},
                    [this](const Trajectory::SharedPtr msg) { on_trajectory(msg); },
                    SubAllocT{});

            m_sub_tf2_ = create_subscription<TFMessage>(
                    tf_topic, rclcpp::QoS{10},
                    [this](const TFMessage::SharedPtr msg) { on_tf(msg); },
                    SubAllocT{});

            m_sub_tf2_static_ = create_subscription<TFMessage>(
                    tf_static_topic, rclcpp::QoS{10},
                    [this](const TFMessage::SharedPtr msg) { on_tf_static(msg); },
                    SubAllocT{});

            using PubAllocT = rclcpp::PublisherOptionsWithAllocator<std::allocator<void>>;
            m_pub_veh_cmd_ = create_publisher<Command>(command_topic, rclcpp::QoS{10}, PubAllocT{});
            m_pub_traj_debug_ = create_publisher<Trajectory>(traj_debug_topic, rclcpp::QoS{10}, PubAllocT{});
            m_debug_cp_ = create_publisher<Pose>(cp_debug_topic, rclcpp::QoS{10}, PubAllocT{});
            m_debug_gp_ = create_publisher<Pose>(gp_debug_topic, rclcpp::QoS{10}, PubAllocT{});

            using namespace std::chrono_literals;
            timer_ = create_wall_timer(
                    100ms, [this] { control_step(); });
        }


        void PidControllerNode::on_state(const State::SharedPtr &msg) {
            m_last_state = std::move(*msg);
        }

        void PidControllerNode::on_tf(const TFMessage::SharedPtr &msg) {
            auto current_tf = msg->transforms.begin();
            if (current_tf->header.frame_id == "odom" &&
                current_tf->child_frame_id == "base_link") {
                m_control_point_radius = static_cast<double>
                (m_last_state.state.longitudinal_velocity_mps);
                if (m_control_point_radius < 5.0) m_control_point_radius = 5.0;
                else if (m_control_point_radius > 7.0) m_control_point_radius = 7.0;
                m_control_point = get_control_point(msg);
                m_odom2base_tf = std::move(*msg);
                //std::cout << "line a : " << line.a << ", b : " << line.b << ", c : " << line.c << "\n";
                m_debug_cp_->publish(m_control_point);
            }
        }

        void PidControllerNode::on_tf_static(const TFMessage::SharedPtr &msg) {

            for (const auto &tr : msg->transforms) {
                if (tr.child_frame_id == "odom" &&
                    tr.header.frame_id == "map") {
                    std::cout << "map to odom statif tf published! \n";
                    m_map2odom_static_tf = std::move(*msg);
                    const auto &tfs = m_map2odom_static_tf.transforms.begin()->transform.translation;
                    std::cout << "x: " << tfs.x;
                    std::cout << ", y : " << tfs.y << "\n";
                }
            }
        }

        void PidControllerNode::on_trajectory(const Trajectory::SharedPtr &msg) {
            m_reference_trajectory = std::move(*msg);
            m_computed_trajectory = get_computed_trajectory(m_reference_trajectory);
            m_pub_traj_debug_->publish(m_computed_trajectory);
        }

        double PidControllerNode::get_distance(const Pose &r1, const Pose &r2) {
            double delta_x = r1.pose.position.x - r2.pose.position.x;
            double delta_y = r1.pose.position.y - r2.pose.position.y;
            return sqrt(pow(delta_x, 2) + pow(delta_y, 2));
        }

        Trajectory PidControllerNode::get_computed_trajectory(const Trajectory &ref_tr) {
            auto ret = ref_tr;
            ret.header.set__frame_id("odom");
            for (size_t i = 0; i < ref_tr.points.size(); i++) {
                const auto &tfs = m_map2odom_static_tf.transforms.begin()->transform.translation;
                ret.points.at(i).x = ref_tr.points.at(i).x - static_cast<float>(tfs.x);
                ret.points.at(i).y = ref_tr.points.at(i).y - static_cast<float>(tfs.y);
            }
            return ret;
        }


        Pose PidControllerNode::get_control_point(const TFMessage::SharedPtr &tf_msg) {

            auto tf = tf_msg->transforms.begin();

            tf2::Quaternion q(
                    tf->transform.rotation.x,
                    tf->transform.rotation.y,
                    tf->transform.rotation.z,
                    tf->transform.rotation.w);
            m_eulers = get_euler_angles(q);

            Pose cp_pose{};
            cp_pose.pose.position.set__x(((tf->transform.translation.x) +
                                          m_control_point_radius * cos(m_eulers.yaw)));
            cp_pose.pose.position.set__y((tf->transform.translation.y) +
                                         m_control_point_radius * sin(m_eulers.yaw));

            cp_pose.pose.set__orientation(tf->transform.rotation);
            cp_pose.set__header(tf->header);

            return cp_pose;
        }

        Pose PidControllerNode::get_goal_point(size_t closest_point_idx_) const {
            Pose goal_point{};
            const auto &traj_points = m_computed_trajectory.points;
            goal_point.header.set__frame_id("odom");
            goal_point.header.set__stamp(this->now());

            goal_point.pose.position.set__x(
                    static_cast<double>(traj_points.at(closest_point_idx_).x));
            goal_point.pose.position.set__y(
                    static_cast<double>(traj_points.at(closest_point_idx_).y));

            goal_point.pose.orientation.set__z(
                    static_cast<double>(traj_points.at(closest_point_idx_).heading.imag));
            goal_point.pose.orientation.set__w(
                    static_cast<double>(traj_points.at(closest_point_idx_).heading.real));

            m_debug_gp_->publish(goal_point);// publish goal point as pose for debugging

            return goal_point;
        }

        float PidControllerNode::calculate_lat_error(const Pose &gp, const Pose &cp) {
            //auto lat_err_unsigned = static_cast<float>(get_distance(gp, cp));
            const auto &line = get_line(cp, m_eulers.yaw);
            const auto &u_lat_error = static_cast<float>(distance_to_line(
                    line, gp.pose.position.x, gp.pose.position.y));
            // calculate the point is which side of the vehicle
            // x1,y1 & x2,y2 are two points of the line that has the same heading with vehicle
            // x1,y1 are the position of base link
            // x2,y2 are the position of control point
            const auto &rtr = m_odom2base_tf.transforms.begin()->transform.translation;
            const auto &x1 = rtr.x;
            const auto &y1 = rtr.y;
            const auto &x2 = cp.pose.position.x;
            const auto &y2 = cp.pose.position.y;
            // x,y are the position of goal point
            const auto &x = gp.pose.position.x;
            const auto &y = gp.pose.position.y;
            auto temp = (x - x1) * (y2 - y1) - (y - y1) * (x2 - x1);
            if (temp > 0) {
                return u_lat_error;
            } else if (temp < 0) {
                return -u_lat_error;
            } else {
                return 0.0;
            }
        }

        float PidControllerNode::calculate_lon_error(size_t goal_traj_idx) const {
             auto ref_vel = m_computed_trajectory.points.at(goal_traj_idx).
                    longitudinal_velocity_mps;

//            ref_vel = utility::clamp(ref_vel,0,8);

            const auto &cur_vel = m_last_state.state.longitudinal_velocity_mps;
            return ref_vel - cur_vel;
        }

        double PidControllerNode::distance_to_line(const Line &rl, double x0, double y0) {
            return abs((rl.a * x0) + (rl.b * y0) + rl.c) / sqrt((rl.a * rl.a) + (rl.b * rl.b));
        }

        size_t PidControllerNode::find_closest_traj() const {
            // get the line which is perpendicular to control point
            const Line line = get_line(m_control_point, m_eulers.yaw - (M_PI / 2));

            bool found_close_enugh_point{false};
            unsigned counter_after_minimum_point{0};
            auto closest_distance{500.0};
            size_t closest_point_idx{};
            const auto &traj_points = m_computed_trajectory.points;
            // find the closest traj point to the line we got
            for (size_t i = 0; i < traj_points.size(); i++) {


                auto dist = distance_to_line(line,
                                             static_cast<double>(traj_points.at(i).x),
                                             static_cast<double>(traj_points.at(i).y));
                if (dist < closest_distance) {
                    if ((closest_distance = dist) < 0.25) { // 0.2 is the distance btwn 2 traj point
                        found_close_enugh_point = true;
                        counter_after_minimum_point = 0;
                    }
                    closest_point_idx = i;
                }
                if (found_close_enugh_point) {
                    if (++counter_after_minimum_point == 2) {
//                        std::cout << "left early return ";
                        return closest_point_idx;
                    }
                }
            }
            return closest_point_idx;
        }

        PidControllerNode::Line PidControllerNode::get_line(const Pose &rps, double angle_rad) {
            const auto &x0 = rps.pose.position.x;
            const auto &y0 = rps.pose.position.y;

            const auto &angle = angle_rad;
            // line equation : ax + by + c = 0
            Line line{};
            line.a = -tan(angle);
            line.b = 1.0;
            line.c = (tan(angle) * x0) - y0;
            return line;
        }


        PidControllerNode::Eulers PidControllerNode::get_euler_angles(tf2::Quaternion &rq) {
            tf2::Matrix3x3 m(rq);
            double roll, pitch, yaw;
            m.getRPY(roll, pitch, yaw);
            return Eulers{roll, pitch, yaw};
        }

        void PidControllerNode::publish_control_command(float str_angle, float long_acc) const {
            Command CmdMsg{};
            CmdMsg.set__stamp(this->now());
            CmdMsg.set__front_wheel_angle_rad(-str_angle);
            CmdMsg.set__long_accel_mps2(long_acc);
            m_pub_veh_cmd_->publish(CmdMsg);
        }

        void PidControllerNode::control_step() {
            if (!m_computed_trajectory.points.empty() &&
                !m_map2odom_static_tf.transforms.empty()) {
                auto closest_point_idx = find_closest_traj();
                m_goal_point = get_goal_point(closest_point_idx);
//                std::cout << "closest_point_idx: " << closest_point_idx << "\n";
                const auto &lateral_error = calculate_lat_error(m_goal_point, m_control_point);
                const auto &str_angle_cmd = LatPID_.calculate(lateral_error);

                const auto &long_error = calculate_lon_error(closest_point_idx);
                const auto &acc_cmd = LonPID_.calculate(long_error);

                publish_control_command(str_angle_cmd, acc_cmd);


                std::cout << "lateral_error = " << lateral_error;
                std::cout << ", str_angle_cmd = " << str_angle_cmd;
                std::cout << ", long_error = " << long_error;
                std::cout << ", acc_cmd = " << acc_cmd << "\n";

            }
        }


    }  // namespace pid_controller
}  // namespace autoware

#include "rclcpp_components/register_node_macro.hpp"

// This acts as an entry point, allowing the component to be
// discoverable when its library is being loaded into a running process
RCLCPP_COMPONENTS_REGISTER_NODE(autoware::pid_controller::PidControllerNode)
